-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: db:3306
-- Generation Time: May 29, 2022 at 05:52 AM
-- Server version: 10.8.3-MariaDB-1:10.8.3+maria~jammy
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ggg`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `position` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `publish_on` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_on` datetime NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` tinyint(1) NOT NULL,
  `display_status` tinyint(1) NOT NULL,
  `update_on` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `status_on` datetime NOT NULL,
  `status_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`position`, `title`, `thumbnail`, `detail`, `publish_on`, `create_by`, `create_on`, `banner_id`, `language_id`, `display_status`, `update_on`, `update_by`, `status_on`, `status_by`) VALUES
('slide', '5.5', 'MjAyMi0wNS0wNl8xNTMzNDhfMjQw.jpeg', '', '2022-05-06 00:00:00', 1, '2022-05-06 15:33:48', 3, 1, 1, NULL, NULL, '2022-05-06 16:30:52', 1),
('slide', 'Check Course', 'MjAyMi0wNS0wNl8xNTM0NTdfNTk1.jpeg', '', '2022-05-27 00:00:00', 1, '2022-05-06 15:34:57', 4, 1, 1, '2022-05-28 09:12:45', 1, '2022-05-06 16:30:50', 1),
('slide', 'Joind', 'MjAyMi0wNS0wNl8xNjMzMTNfNzU0.jpeg', '', '2022-05-06 00:00:00', 1, '2022-05-06 16:33:13', 5, 1, 1, '2022-05-06 16:33:21', 1, '2022-05-28 10:30:47', 1),
('promotion', '1', 'MjAyMi0wNS0yOF8xMDM2MzBfNjU0.jpeg', '', '2022-05-28 00:00:00', 1, '2022-05-28 10:36:30', 6, 1, 1, NULL, NULL, '0000-00-00 00:00:00', 0),
('promotion', '2', 'MjAyMi0wNS0yOF8xMDM3MzNfMzcw.jpeg', '', '2022-05-27 00:00:00', 1, '2022-05-28 10:37:33', 7, 1, 1, NULL, NULL, '0000-00-00 00:00:00', 0),
('top', 'Top', 'MjAyMi0wNS0yOF8xMDQzNThfOTY0.png', '', '2022-05-01 00:00:00', 1, '2022-05-28 10:43:58', 8, 1, 1, NULL, NULL, '0000-00-00 00:00:00', 0),
('icon', 'Golf Go Go', 'MjAyMi0wNS0yOF8xMDQ3MDVfOTQ2.png', '', '2022-05-01 00:00:00', 1, '2022-05-28 10:47:05', 9, 1, 1, NULL, NULL, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `service` enum('golfgogo') NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `create_on` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_on` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `status_on` datetime NOT NULL,
  `status_by` int(11) NOT NULL,
  `language_id` tinyint(1) NOT NULL,
  `display_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `title`, `service`, `thumbnail`, `create_on`, `create_by`, `update_on`, `update_by`, `status_on`, `status_by`, `language_id`, `display_status`) VALUES
(1, '1', 'golfgogo', 'MjAyMi0wNS0wNl8xNzQwMDJfODE0.jpeg', '2022-05-06 17:40:02', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 1, 1),
(2, '2.', 'golfgogo', 'MjAyMi0wNS0wNl8xNzQxMDdfNzEw.jpeg', '2022-05-06 17:41:07', 1, '2022-05-06 17:41:42', 1, '2022-05-06 17:41:21', 1, 1, 1),
(3, '3', 'golfgogo', 'MjAyMi0wNS0wNl8xNzQzNTlfMzA5.jpeg', '2022-05-06 17:43:59', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone_no` varchar(10) NOT NULL,
  `subscription` enum('y','n') DEFAULT NULL,
  `policy` enum('y','n') DEFAULT NULL,
  `create_on` datetime NOT NULL,
  `update_on` timestamp NULL DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `point` int(11) NOT NULL,
  `birth_day` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `firstname`, `lastname`, `phone_no`, `subscription`, `policy`, `create_on`, `update_on`, `image`, `point`, `birth_day`) VALUES
(24, 'Suttipong', 'Ruanglitt', '0909861366', 'y', 'y', '2022-05-29 03:19:50', NULL, '', 0, '1989-03-26 00:00:00'),
(25, 'Nickyy', 'Nakrub', '0909877762', 'n', 'y', '2022-05-29 03:28:05', '2022-05-29 03:38:37', 'https://dev.www.ggg-inter.com/static/upload/img/MjAyMi0wNS0yOV8xMDMzNTlfOTM1.png', 0, '1999-02-26 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `force_update`
--

CREATE TABLE `force_update` (
  `force_update_id` int(11) NOT NULL,
  `version` varchar(255) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_on` datetime NOT NULL,
  `display_status` tinyint(1) NOT NULL,
  `update_by` int(11) NOT NULL,
  `update_on` datetime NOT NULL,
  `language_id` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `force_update`
--

INSERT INTO `force_update` (`force_update_id`, `version`, `create_by`, `create_on`, `display_status`, `update_by`, `update_on`, `language_id`) VALUES
(1, '0.0.2', 1, '2022-05-29 11:59:00', 1, 1, '2022-05-29 12:05:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_slug` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`language_id`, `language_name`, `language_slug`) VALUES
(1, 'Thailand', 'th');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `member_id` int(11) NOT NULL,
  `group_id` smallint(2) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `telephone` varchar(10) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_on` datetime DEFAULT NULL,
  `login_on` datetime DEFAULT NULL,
  `logout_on` datetime DEFAULT NULL,
  `update_on` datetime DEFAULT NULL,
  `change_password_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `group_id`, `parent_id`, `username`, `password`, `salt`, `name`, `lastname`, `telephone`, `location_id`, `status`, `create_on`, `login_on`, `logout_on`, `update_on`, `change_password_on`) VALUES
(1, 1, 0, 'nick', '4823105c4f01b8cb0cae40d4935cfaeb', 'f79b243766171f9417c50dd20e2f1b0d', 'Suttipong', 'Ruanglitt', NULL, 0, 1, '2020-03-26 00:00:00', '2022-05-29 11:37:07', '2022-05-29 12:06:09', '2022-05-29 12:06:09', '2022-04-20 17:57:59'),
(18, 2, 0, 'admin', '23720e6bbe65dbde7d1a6ca632e25782', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'GGG', '0909861367', NULL, 1, '2022-04-20 17:55:10', '2022-05-29 12:06:16', '2022-05-29 11:36:54', '2022-05-29 12:06:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `member_group`
--

CREATE TABLE `member_group` (
  `group_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `allow_menu` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `member_group`
--

INSERT INTO `member_group` (`group_id`, `name`, `allow_menu`) VALUES
(1, 'Developer', NULL),
(2, 'Admin', '[\"1\",\"25\",\"26\",\"27\",\"28\",\"15\",\"17\",\"21\",\"22\"]');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icon_name` varchar(255) DEFAULT NULL,
  `is_toggle` tinyint(1) NOT NULL,
  `parent` int(11) NOT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `slug`, `name`, `url`, `icon_name`, `is_toggle`, `parent`, `order`) VALUES
(1, 'dashboard', 'Dashboard', NULL, '1.png', 0, 0, 1),
(2, 'member', 'Member', NULL, '5.png', 1, 0, 99),
(3, 'add_member', 'Add Member', 'member/add', NULL, 0, 2, 1),
(4, 'manage_member', 'Manage Member', 'member/manage', NULL, 0, 2, 1),
(5, 'group_member', 'Group Member', 'member/group/list', NULL, 0, 2, 2),
(15, 'banner', 'Banner', NULL, '8.png', 1, 0, 3),
(17, 'manage_banner', 'Manage Banner', 'banner/manage', NULL, 0, 15, 1),
(21, 'page', 'Page', NULL, '8.png', 1, 0, 3),
(22, 'manage_page', 'Manage Page', 'pages/manage', NULL, 0, 21, 1),
(23, 'coupon', 'Coupon', NULL, '8.png', 1, 0, 3),
(24, 'manage_coupon', 'Manage Coupon', 'coupons/manage', NULL, 0, 23, 1),
(25, 'customer', 'Customer', NULL, '8.png', 1, 0, 2),
(26, 'manage_customer', 'Manage Customer', 'customers/manage', NULL, 0, 25, 1),
(27, 'force_update', 'Force Update', NULL, '8.png', 1, 0, 2),
(28, 'manage_force_update', 'Manage Force update', 'force_update/manage', NULL, 0, 27, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `pages_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `create_on` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_on` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `detail` text NOT NULL,
  `language_id` tinyint(1) NOT NULL,
  `display_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`pages_id`, `title`, `create_on`, `create_by`, `update_on`, `update_by`, `detail`, `language_id`, `display_status`) VALUES
(1, 'Privacy Policy', '2022-05-06 17:12:40', 1, '0000-00-00 00:00:00', 0, '<p>Privacy Policy<br></p>', 1, 1),
(2, 'Term and Condition', '2022-05-06 17:13:39', 1, '2022-05-06 17:21:16', 18, '<p>Term and Condition</p><p><br></p><p><img src=\"https://dev.www.ggg-inter.com/static/upload/img_content/MjAyMi0wNS0wNl8xNzE3MTRfNjQ3.jpg\" style=\"width: 1080px;\" data-filename=\"20220505103658249.jpeg\"><br></p>', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`banner_id`),
  ADD KEY `create_by` (`create_by`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `display_status` (`display_status`),
  ADD KEY `status_by` (`status_by`),
  ADD KEY `update_by` (`update_by`),
  ADD KEY `position` (`position`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`),
  ADD KEY `service` (`service`),
  ADD KEY `create_by` (`create_by`),
  ADD KEY `update_by` (`update_by`),
  ADD KEY `status_by` (`status_by`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `display_status` (`display_status`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone_no` (`phone_no`),
  ADD KEY `subscription` (`subscription`),
  ADD KEY `policy` (`policy`);

--
-- Indexes for table `force_update`
--
ALTER TABLE `force_update`
  ADD PRIMARY KEY (`force_update_id`),
  ADD KEY `create_by` (`create_by`),
  ADD KEY `update_by` (`update_by`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`),
  ADD UNIQUE KEY `username_2` (`username`),
  ADD KEY `username` (`username`),
  ADD KEY `salt` (`salt`),
  ADD KEY `telephone` (`telephone`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `status` (`status`),
  ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `member_group`
--
ALTER TABLE `member_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `url` (`url`),
  ADD KEY `name` (`name`),
  ADD KEY `parent` (`parent`) USING BTREE,
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pages_id`),
  ADD KEY `update_by` (`update_by`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `create_by` (`create_by`),
  ADD KEY `display_status` (`display_status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `force_update`
--
ALTER TABLE `force_update`
  MODIFY `force_update_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `member_group`
--
ALTER TABLE `member_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `pages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
