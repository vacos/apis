# Run Go on Local
1. run "docker-compose up --build"
2. open browser for phpmyadmin "http://localhost:5003"
3. run "curl http://localhost:8000"
{"message":"Service is running."}

# Run Go On Production
1. run "docker build -t ggg ."
2. run "docker run -p 8000:8000 ggg"
3. open browser for phpmyadmin "http://golfgogo.me/phpmyadmin/"
3. run "curl http://139.99.16.121:8000"
{"message":"Service is running."}